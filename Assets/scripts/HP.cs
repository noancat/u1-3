﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HP : MonoBehaviour
{
    [SerializeField] GameObject destroyed;
    [SerializeField] private int hp;
    Animator animator;
    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    public void Init(int health)
    {
        hp = health;
    }
    public void GetDamage(int dmg)
    {
        hp -= dmg;
        if (animator != null)
            animator.SetInteger("state", 4);
        if (hp <= 0)
        {
            if (animator != null)
            {
                animator.SetInteger("state", 6);
            }
            if (destroyed != null)
            {
                GameObject.Instantiate(
                    destroyed,
                    transform.position,
                    transform.rotation
                    //,transform.parent
                    );
                Destroy(gameObject);
            }
        }            
    }
    public int GetHP()
    {
        return hp;
    }
}
