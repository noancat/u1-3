﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Resource : InventoryItem
{
    public bool canStack;
    public int maxStack;
}
