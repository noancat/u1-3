﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RangeWeapon : Weapon
{
    [Header("Shooting")]
    [SerializeField] GameObject bulletPrefab;
    [SerializeField] int distance;
    [SerializeField] Transform shootPoint;

    [Header("Reloading")]
    [SerializeField] int reloadTime;

    [Header("Ammo")]
    [SerializeField] int currentClip;
    [SerializeField] int totalAmmo;
    [SerializeField] int clipCapacity;

    public override void Attack(RaycastHit target)
    {
        //base.Attack(target); // no pew!
        if (currentClip == 0) return;

        GameObject bullet = GameObject.Instantiate(
            bulletPrefab,
            shootPoint.position,
            shootPoint.rotation
        );
        bullet.GetComponent<Rigidbody>().AddForce(bullet.transform.forward * 30f, ForceMode.Impulse);
        Destroy(bullet, 5.0f);
        currentClip--;
        UpdateUI();
    }
    public void Reload()
    {
        int demand = clipCapacity - currentClip;

        if (totalAmmo >= demand)
        {
            totalAmmo -= demand;
            currentClip = clipCapacity;
        }
        else
        {
            currentClip += totalAmmo;
            totalAmmo = 0;
        }
        UpdateUI();
    }
    void UpdateUI()
    {
        GameManager.Instance().ammoText.text = currentClip + " / " + totalAmmo;
    }
}
