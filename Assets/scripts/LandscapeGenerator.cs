﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class LandscapeGenerator : MonoBehaviour
{
    public GameObject[] prefabs;
    public int size;
    public int maxH;

    public GameObject spawner;

    public void Create()
    {
        int seed = Random.Range(0, 16);
        for (int j = 0; j < size; j++)
        {
            for (int i = 0; i < size; i++)
            {
                int y = (int)
                    (maxH * Mathf.PerlinNoise((i + seed) / 32f, (j + seed) / 32f));

                //for (int a = y; a >= 0; a--)
                //{
                int k = (int)(y * prefabs.Length / (1f * maxH));
                GameObject.Instantiate(
                    prefabs[k],
                    new Vector3(i, y, j), //0,0,0
                    Quaternion.identity, //0,0,0
                    transform
                    );
                //}
            }
        }
        GetComponent<NavMeshSurface>().BuildNavMesh();

        int index = Random.Range(0, transform.childCount);
        spawner.transform.position = (transform.GetChild(index).position + Vector3.up * 0.5f);

        // place spawner above random block
    }
    public void Load(List<string> lines)
    {
        foreach (string line in lines)
        {
            string[] tmp = line.Split(';');
            int x = int.Parse(tmp[0]);
            int y = int.Parse(tmp[1]);
            int z = int.Parse(tmp[2]);
            int id = int.Parse(tmp[3]);
            int hp = int.Parse(tmp[4]);
            GameObject block = GameObject.Instantiate(
                    prefabs[id-1],
                    new Vector3(x, y, z), //0,0,0
                    Quaternion.identity, //0,0,0
                    transform
                    );
            block.GetComponent<HP>().GetDamage(
                block.GetComponent<HP>().GetHP() - hp
                );
            //hp.Init();
        }
    }
}
