﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelController : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // мой самый крутой метод update:
    void Update()
    {
        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");

        Quaternion rot = transform.rotation;
        rot.x += v * 0.02f;
        rot.z -= h * 0.02f;
        transform.rotation = rot;
    }
}
