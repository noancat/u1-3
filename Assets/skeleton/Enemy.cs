﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Enemy : MonoBehaviour
{
    [Range(1, 30)] [SerializeField] float speed = 10f;
    // Start is called before the first frame update
    CharController player;
    Animator animator;
    NavMeshAgent agent;

    void Start()
    {
        animator = GetComponent<Animator>();
        agent = GetComponent<NavMeshAgent>();
    }

    // Update is called once per frame
    void Update()
    {
        //transform.Rotate(transform.up * Time.deltaTime * 10);
        if (player != null)
        {
            /*
            //rotate at player
            transform.LookAt(player.transform);
            Vector3 tmp = new Vector3(0, transform.rotation.eulerAngles.y, 0);
            transform.rotation = Quaternion.Euler(tmp);

            // move to player
            transform.Translate(transform.forward * speed * Time.deltaTime);
            */

            agent.SetDestination(player.transform.position);

            if ((player.transform.position - transform.position).magnitude <= 2f)
            {
                // attack
                agent.isStopped = true;
                animator.SetInteger("state", 3);
            }
            else
            {
                // walk
                agent.isStopped = false;
                animator.SetInteger("state", 1);
            }
        }
        else
        {
            // idle
            agent.isStopped = true;
            animator.SetInteger("state", 0);
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<CharController>()) // != null
            player = other.GetComponent<CharController>();
    }
    private void OnTriggerExit(Collider other)
    {
        if (other.GetComponent<CharController>()) // != null
            player = null;
    }

    public void Attack(int dmg)
    {
        print("aaggrh!!11 ");
        player.GetComponent<HP>().GetDamage(dmg);
    }
}
